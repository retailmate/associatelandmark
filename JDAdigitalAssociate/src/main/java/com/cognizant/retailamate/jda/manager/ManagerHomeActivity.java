package com.cognizant.retailamate.jda.manager;

/**
 * Created by Bharath on 11/26/2016.
 */


import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailamate.jda.R;
import com.cognizant.retailamate.jda.activity.SettingsActivity;


public class ManagerHomeActivity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    ImageView tick1View, tick2View, tick3View, nav_header_pic;
    TextView taskView, totalTaskView;

    final float growTo = .1f;
    final long duration = 500;

    View hView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manager_home);

        final ImageView group = (ImageView) findViewById(R.id.group);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tick1View = (ImageView) findViewById(R.id.tick1);
        tick2View = (ImageView) findViewById(R.id.tick2);
        tick3View = (ImageView) findViewById(R.id.tick3);


        setSupportActionBar(toolbar);
        initNavigationDrawer();

        taskView = (TextView) findViewById(R.id.task_count);
        totalTaskView = (TextView) findViewById(R.id.total_task_count);


        final RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(0);
        anim.setDuration(700);


        ScaleAnimation grow = new ScaleAnimation(1, growTo, 1, growTo,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        grow.setDuration(duration / 2);
        ScaleAnimation shrink = new ScaleAnimation(growTo, 1, growTo, 1,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(duration / 2);
        shrink.setStartOffset(duration / 2);
        AnimationSet growAndShrink = new AnimationSet(true);
        growAndShrink.setInterpolator(new LinearInterpolator());
        growAndShrink.addAnimation(shrink);


        tick1View.startAnimation(growAndShrink);
        tick2View.startAnimation(growAndShrink);
        tick3View.startAnimation(growAndShrink);

        ValueAnimator animator = new ValueAnimator();
        animator.setObjectValues(0, 7);
        animator.setDuration(1000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                taskView.setText("" + (int) animation.getAnimatedValue());
                if ((int) animation.getAnimatedValue() == 5) {
//                    tick1View.setVisibility(View.VISIBLE);
//                    tick1View.startAnimation(growAndShrink);
//                    tick1View.setImageResource(R.drawable.tick);
                }
                if ((int) animation.getAnimatedValue() == 6) {
//                    tick2View.setVisibility(View.VISIBLE);
//                    tick2View.startAnimation(growAndShrink);
//                    tick2View.setImageResource(R.drawable.tick);
                }
                if ((int) animation.getAnimatedValue() == 7) {
//                    tick3View.setVisibility(View.VISIBLE);
//                    tick3View.startAnimation(growAndShrink);
//                    tick3View.setImageResource(R.drawable.wrong);
                }
            }
        });
        animator.start();

        ValueAnimator animator1 = new ValueAnimator();
        animator1.setObjectValues(0, 10);
        animator1.setDuration(1000);
        animator1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                totalTaskView.setText("" + (int) animation.getAnimatedValue());
            }
        });
        animator1.start();

        group.setAnimation(anim);

        nav_header_pic.setImageResource(R.drawable.john);

    }

    public void initNavigationDrawer() {


        final NavigationView navigationView = (NavigationView) findViewById(R.id.manager_navigation_view);

        hView = navigationView.getHeaderView(0);

        nav_header_pic = (ImageView) hView.findViewById(R.id.nav_header_pic);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                int id = menuItem.getItemId();

                switch (id) {
                    case R.id.manager_my_tasks:
                        Toast.makeText(getApplicationContext(), "My Tasks", Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.manager_my_store_view:


                        Intent intent = new Intent(ManagerHomeActivity.this, HeatMapActivity.class);
                        // if intent string is "allassociates", api call is made else UI is updated with the global hashmap
                        // since this is the first intent to heatmap activity, "allassociates" is used
                        intent.putExtra("flag", "allAssociates");
                        startActivity(intent);
                        drawerLayout.closeDrawers();
                        break;


                    case R.id.manager_sales_analytics:
                        Toast.makeText(getApplicationContext(), "Sales Analytics", Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.manager_settings:
                        Intent intent1 = new Intent(ManagerHomeActivity.this, SettingsActivity.class);
                        intent1.putExtra("userrole", "manager");
                        startActivity(intent1);
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.manager_my_profile:
                        Toast.makeText(getApplicationContext(), "My Profile", Toast.LENGTH_SHORT).show();
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.manager_logout:
                        finish();

                }
                return true;
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.manager_drawer);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View v) {
                super.onDrawerClosed(v);
            }

            @Override
            public void onDrawerOpened(View v) {
                super.onDrawerOpened(v);
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


}
