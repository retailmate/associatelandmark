package com.cognizant.retailamate.jda.utils;

/**
 * Created by 452781 on 12/2/2016.
 */
public class Constants {

    public static String arabic = "عربى";
    public static String english = "English";


    public static String azureJDALoginURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/GetAssociateDetails?LoginName=";
    public static String azureLoginURL = "http://rmlmapi.azurewebsites.net/Api/MovementServices/ValidateUserAPI?userName=";
    public static String getAssociateDetailsURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/GetAssociateScheduleAPI?associateId=";
    public static String getAllAssociatesURL = "http://rmlmapi.azurewebsites.net/Api/MovementServices/GetAllAssociatesPositionAPI";
    public static String getAllCustomerURL = "http://rmlmapi.azurewebsites.net/Api/MovementServices/GetAllCustomerPositionAPI";
    public static String getAssociateJobURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/GetAssociateTasksAPI?associateId=";
    public static String updateAssociateTaskURL = "http://jdaretailmateapidev.azurewebsites.net/api/JDAServices/UpdateTaskAPI?taskId=";

    public static String SignalRURL = "http://rmlmapi.azurewebsites.net/";
    public static String SignalRHubProxy = "LMHubServer";
}
