package com.cognizant.retailamate.jda.associate;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cognizant.retailamate.jda.R;

public class CustomerFeedback extends AppCompatActivity {

    TextView next_customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_feedback_popup);

        next_customer = (TextView) findViewById(R.id.next_customer);

        next_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerFeedback.this,Next_customer.class);
                startActivity(intent);
            }
        });
    }
}
