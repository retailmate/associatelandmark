package com.cognizant.retailamate.jda.Model;

/**
 * Created by Bharath on 11/26/2016.
 */

public class AssociatePersonalDetailsModel {

    /**
     * AssociateId : 1000236
     * StartTime : 2016-12-01T13:00:00
     * EndTime : 2016-12-01T22:00:00
     * SiteId : 1000553
     * JobName : Receiving Clerk
     * CaptureDate : 2016-12-01T00:00:00
     */

    private int AssociateId;
    private String StartTime;
    private String EndTime;
    private int SiteId;
    private String JobName;
    private String CaptureDate;

    public int getAssociateId() {
        return AssociateId;
    }

    public void setAssociateId(int AssociateId) {
        this.AssociateId = AssociateId;
    }

    public String getStartTime() {
        return StartTime;
    }

    public void setStartTime(String StartTime) {
        this.StartTime = StartTime;
    }

    public String getEndTime() {
        return EndTime;
    }

    public void setEndTime(String EndTime) {
        this.EndTime = EndTime;
    }

    public int getSiteId() {
        return SiteId;
    }

    public void setSiteId(int SiteId) {
        this.SiteId = SiteId;
    }

    public String getJobName() {
        return JobName;
    }

    public void setJobName(String JobName) {
        this.JobName = JobName;
    }

    public String getCaptureDate() {
        return CaptureDate;
    }

    public void setCaptureDate(String CaptureDate) {
        this.CaptureDate = CaptureDate;
    }
}
