package com.cognizant.retailamate.jda.chatbot;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cognizant.retailamate.jda.R;

import java.util.List;


/**
 * Created by 543898 on 9/30/2016.
 **/
public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private static final String TAG = "CustomAdapter";


    List<ChatDataModel> chatDataModelList;
    private static final String CustomTag = "CustomTag";
    private Context mContext;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View v) {
            super(v);
        }
    }

    public class SendViewHolder extends ViewHolder {
        TextView sendMessage;
        TextView time;

        public SendViewHolder(View v) {
            super(v);
            this.sendMessage = (TextView) v.findViewById(R.id.textview_message_send);
            this.time = (TextView) v.findViewById(R.id.textview_time);
        }
    }

    public class ReceiveViewHolder extends ViewHolder {
        TextView receiveMessage;
        TextView time;

        public ReceiveViewHolder(View v) {
            super(v);
            this.receiveMessage = (TextView) v.findViewById(R.id.textview_message_receive);
            this.time = (TextView) v.findViewById(R.id.textview_time);
        }
    }


    public ChatAdapter(Context context, List<ChatDataModel> chatDataList) {

        this.chatDataModelList = chatDataList;
        this.mContext = context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        Log.e("TAG", "(o)_(o)" + viewType);
        View v;
        if (viewType == ChatGlobal.SEND) {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.chatbot_chat_user_send, viewGroup, false);

            return new SendViewHolder(v);
        } else {
            v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.chatbot_chat_user_receive, viewGroup, false);
            return new ReceiveViewHolder(v);
        }

    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        if (viewHolder.getItemViewType() == ChatGlobal.SEND) {
            SendViewHolder holder = (SendViewHolder) viewHolder;
            holder.sendMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        } else if (viewHolder.getItemViewType() == ChatGlobal.RECEIVE) {
            ReceiveViewHolder holder = (ReceiveViewHolder) viewHolder;
            holder.receiveMessage.setText(chatDataModelList.get(position).getmDataset());
            holder.time.setText(chatDataModelList.get(position).getmTime());
        }
    }

    @Override
    public int getItemCount() {
        Log.e("TAG", "(o)_(o)" + chatDataModelList.size());
        return chatDataModelList.size();
    }

    @Override
    public int getItemViewType(int position) {
//
        return chatDataModelList.get(position).getmDatasetTypes();
    }


}
