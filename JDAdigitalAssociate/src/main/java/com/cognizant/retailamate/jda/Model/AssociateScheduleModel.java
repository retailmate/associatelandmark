package com.cognizant.retailamate.jda.Model;

import java.util.ArrayList;

/**
 * Created by 452781 on 12/5/2016.
 */
public class AssociateScheduleModel {

    ArrayList<AssociatePersonalDetailsModel> associateSchedule = new ArrayList<AssociatePersonalDetailsModel>();


    public ArrayList<AssociatePersonalDetailsModel> getAssociateSchedule() {
        return associateSchedule;
    }

    public void setAssociateSchedule(ArrayList<AssociatePersonalDetailsModel> associateSchedule) {
        this.associateSchedule = associateSchedule;
    }
}
