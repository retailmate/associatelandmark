package com.cognizant.retailamate.jda.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AzureLoginModel {

    @SerializedName("SiteId")
    @Expose
    private String siteId;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}