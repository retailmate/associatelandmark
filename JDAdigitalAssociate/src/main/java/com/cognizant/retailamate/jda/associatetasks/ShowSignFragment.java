package com.cognizant.retailamate.jda.associatetasks;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.cognizant.retailamate.jda.R;

/**
 * Created by 543898 on 12/5/2016.
 */
public class ShowSignFragment extends AppCompatActivity {

    Boolean flag;
    ImageView imageView;
    TextView textView;
    final float growTo = .1f;
    final long duration = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tick_pop_up);


        flag = getIntent().getBooleanExtra("flag", Boolean.FALSE);


        imageView = (ImageView) findViewById(R.id.imageV);
        textView = (TextView) findViewById(R.id.match);
        if (flag) {
            imageView.setImageResource(R.drawable.tick);
            textView.setText("Product matched.");
        } else if (!flag) {
            imageView.setImageResource(R.drawable.wrong);
            textView.setText("Wrong product. Please try again.");

        }
        ScaleAnimation grow = new ScaleAnimation(1, growTo, 1, growTo,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        grow.setDuration(duration / 2);
        ScaleAnimation shrink = new ScaleAnimation(growTo, 1, growTo, 1,
                Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        shrink.setDuration(duration / 2);
        shrink.setStartOffset(duration / 2);
        AnimationSet growAndShrink = new AnimationSet(true);
        growAndShrink.setInterpolator(new LinearInterpolator());
        growAndShrink.addAnimation(shrink);
        imageView.startAnimation(growAndShrink);
    }

    public void closeFragment(View view) {
        finish();
    }
}